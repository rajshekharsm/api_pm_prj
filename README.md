# api_pm_prj



## Introduction

This project is about automating REST API methods like PUT, POST, GET, PATCH in postman.

## Features

****Data Driven Testing:************

When we want to test different data then we can use data driven testing concept with the help of excel sheet or json file.
1. Data driven testing using excel sheet:
- Create a excel sheet with column name as per requestbody contents and enter the values in columns and save in CSV format.
- Create a global variables or collection variable as our columns names in excel sheet.
- Write a test code with required method.
- Run the collection 
- Select the excel file run.
2. Data driven testing using JSON:
- Create JSON file as per requestbody contents 
- Create a global variables or collection variable as our columns names in excel sheet.
- Write a test code with required method.
- Run the collection 
- Select the json file and run.

**Dynamic Variables: **
This concept used for generate different types of test data with the help of faker library, 
- write a code in pre request script.
- Run the collection and enter iteration count as per need.

**Request Chaining:**
Here we can define the flow of execution of API.

**Newman Tool:**
Here, we are generating reports in html and html extra using newman tool.
HTML report give details as collection name, time (which shows the date and time of report generated), Exported with (shows the newman tool version used for report generation) and all details of collections with method wise having  pass count and fail count, Total run duration, Total data received, Average response time.
HTMLextra report in which we can get detail report i.e. summary, total requests, failed cases, skipped cases.
HTMLextra report is more user friendly report than html report.
**Steps for HTML report:**
In Postman
1.	Export collection and environment and save in local drive.
2.	In CMD type command as 
newman run path of exported collection (.json) file –e path of exported environment (.json) file –reporters html –reporter-html-export path to save report with file name

eg. I:\MS\newman newman run C:\Users\Raj\Downloads\Rest_API_reference.postman_collection.json -e I:\MS\newman\REQRES_ENV.postman_environment.json --reporters html --reporter -html-export I:\MS\newman\newman_html_report.html

3.	Will get .html file in targeted folder.

**Steps for HTMLextra report:**
In Postman
1.	Export collection and environment and save in local drive.
2.	In CMD type command as 
newman run path of exported collection (.json) file –e path of exported environment (.json) file –reporters htmlextra –reporter-htmlextra-export path to save report with file name

eg. I:\MS\newman newman run C:\Users\Raj\Downloads\Rest_API_reference.postman_collection.json -e I:\MS\newman\REQRES_ENV.postman_environment.json --reporters htmlextra --reporter -htmlextra-export I:\MS\newman I:\MS\newman_htmlextra.html
3.	Will get .html file in targeted folder.

